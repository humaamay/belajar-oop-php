<?php

require_once 'Ape.php';
require_once 'Frog.php';

$sheep = new Animal('shaun');

echo $sheep->getName() . '<br/>';
echo $sheep->getLegs() . '<br/>';
echo $sheep->getColdBlooded() . '<br/>';

// index.php
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

echo '<br/>';

$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"

class Animal
{
    private $name;
    private $legs;
    private $coldBlooded;

    public function __construct($name)
    {
        $this->name = $name;
        $this->legs = 2;
        $this->coldBlooded = false;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLegs()
    {
        return $this->legs;
    }

    public function getColdBlooded()
    {
        return (string) $this->coldBlooded;
    }
}